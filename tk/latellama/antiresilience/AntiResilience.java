package tk.latellama.antiresilience;

import org.bukkit.plugin.java.JavaPlugin;
import tk.latellama.antiresilience.listeners.JoinListener;

import java.util.logging.Logger;

public class AntiResilience extends JavaPlugin {
    private final Logger log = getLogger();
    private final JoinListener joinListener = new JoinListener(this);

    @Override
    public void onEnable() {
        log.info("AntiResilience enabled, resilience users should no longer be able to join your server.");
        getServer().getPluginManager().registerEvents(joinListener, this);
    }
}
