package tk.latellama.antiresilience.listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import tk.latellama.antiresilience.AntiResilience;
import tk.latellama.antiresilience.threads.CheckThread;

public class JoinListener implements Listener {
    private final AntiResilience plugin;

    public JoinListener(final AntiResilience plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public final void onPlayerJoin(final PlayerJoinEvent event) {
        final CheckThread thread = new CheckThread(event.getPlayer(), plugin);
        Bukkit.getScheduler().runTask(plugin, thread);
    }
}
