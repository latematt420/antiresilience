package tk.latellama.antiresilience.threads;

import org.bukkit.entity.Player;
import tk.latellama.antiresilience.AntiResilience;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class CheckThread extends Thread implements Runnable {
    private final Player player;
    private final AntiResilience plugin;
    public CheckThread(final Player player, final AntiResilience plugin) {
        this.player = player;
        this.plugin = plugin;
    }

    public void run() {
        boolean isUsing = false;
        try {
            final URL url = new URL("http://resilience.krispdev.com/onlineMSE");
            final BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.equals("")) continue;
                if (line.contains(player.getName())) {
                    isUsing = true;
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (isUsing) {
            plugin.getLogger().warning(player.getName() + " is using Resilience. Blocked join.");
            player.kickPlayer("Do not use Resilience.");
        }
    }
}
